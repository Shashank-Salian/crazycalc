import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const Button = (props) => {
	return (
		<TouchableOpacity activeOpacity={0.8} style={{...styles.btn, ...props.style}} onPress={props.onClick}>
			<Text style={styles.txt}>{props.children}</Text>
		</TouchableOpacity>
	)
}

export default Button

const styles = StyleSheet.create({
	btn: {
		width: 75,
		height: 75,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#2F2F2F',
		margin: '2.5%',
		borderRadius: 50
	},
	txt: {
		color: '#fff',
		fontSize: 36
	}
})
