/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, { useState, useRef, useEffect } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import BtnContainer from './container/BtnContainer';

const App = () => {
  const [num1, setNum1] = useState()
  const [num2, setNum2] = useState()
  const [sym, setSym] = useState()
  const [otherTxt, setOtherTxt] = useState("")

  const [numbers, setNumbers] = useState([9, 8, 7, 6, 5, 4, 3, 2, 1, 0, '='])
  const [symbols, setSymbols] = useState(['÷', '×', '-', '+'])

  const scrollViewRef = useRef()
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? '#333' : '#eee',
  };

  const onBtnClick = (btn) => {
    if (btn >= 0 && btn <= 9) {
      if (!sym)
        setNum1(`${num1 ? num1 : ''}${btn}`)
      else
        setNum2(`${num2 ? num2 : ''}${btn}`)
      setNumbers(oldState => oldState.shuffle())
    } else if (btn === '÷' || btn === '×' || btn === '+' || btn === '-') {
      setSymbols(oldSym => oldSym.shuffle())
      setSym(btn)
    } else if (btn === '=') {
      setNum1(calc())
      setSym(null)
    } else if (btn === 'C') {
      setSym(null)
      setNum1(null)
      setNum2(null)
    }

    scrollViewRef.current.scrollToEnd({ animated: true, duration: 600 })
  }

  const calcRight = (n1, n2) => {
    switch (sym) {
      case '+':
        return n1 + n2
      case '-':
        return n1 - n2
      case '÷':
        return n1 / n2
      case '×':
        return n1 * n2
      default:
        break
    }
  }

  const calcWrong = (n1, n2) => {
    n1 = n1 + Math.floor(Math.random()*n1 + Math.random()*20)
    n2 = n2 + Math.floor(Math.random()*n2 + Math.random()*20)
    switch (sym) {
      case '÷':
        return n1 + n2
      case '×':
        return n1 - n2
      case '+':
        return n1 / n2
      case '-':
        return n1 * n2
      default:
        break
    }
  }

  const calc = () => {
    const n1 = parseInt(num1), n2 = parseInt(num2)
    
    setNum1(null)
    setNum2(null)

    if (n1 <= 5 && n2 <= 5) {
      setOtherTxt('Lol! You really need a calculator. Dumbass')
      return calcRight(n1, n2)
    }

    const rand = Math.floor((Math.random()*100)+1)
    if (rand < 31) {
      return calcRight(n1, n2)
    } else {
      // return calcRight(n1, n2)
      return calcWrong(n1, n2)
    }
  }

  useEffect(() => {
    const id = setInterval(() => {
      setNumbers(oldNum => oldNum.shuffle())
      setSymbols(oldSym => oldSym.shuffle())
    }, 2000)

    return () => {
      clearInterval(id)
    }
  }, [])

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View style={styles.result}>
        <View style={styles.topResult}>
          <View style={styles.otherTxtContainer}>
            <Text style={styles.otherTxt}>{otherTxt}</Text>
          </View>
          <View style={styles.symContainer}>
            <Text style={styles.symbolTxt}>{sym}</Text>
          </View>
        </View>
        <View style={styles.resultContainer}>
          <ScrollView horizontal={true} contentContainerStyle={styles.scrollView} ref={scrollViewRef}>
            <Text style={styles.textContainer}>{num2 ? num2 : num1}</Text>
          </ScrollView>
        </View>
      </View>
      <View style={styles.btnContainer}>
        <BtnContainer
          onBtnClick={onBtnClick}
          numbers={numbers}
          symbols={symbols} />
      </View>
    </SafeAreaView>
  );
};

Array.prototype.shuffle = function () {
  let array = [...this]
  let currentIndex = array.length, temporaryValue, randomIndex;
  
  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}

const styles = StyleSheet.create({
  result: {
    height: '30%',
    justifyContent: 'space-between',
    backgroundColor: '#fff'
  },
  resultContainer: {
    height: '65%',
    alignItems: 'flex-end',
    paddingHorizontal: 15,
    paddingVertical: 20
  },
  topResult: {
    flex: 1,
    flexDirection: 'row',
  },
  otherTxtContainer: {
    flex: 1
  },
  otherTxt: {
    fontSize: 22
  },
  symContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingVertical: 20,
    paddingHorizontal: 15,
    width: '15%'
  },
  symbolTxt: {
    fontSize: 36
  },
  scrollView: {
    alignItems: 'flex-end'
  },
  textContainer: {
    fontSize: 52
  },
  btnContainer: {
    height: '70%',
    width: '100%',
    backgroundColor: '#fff'
  }
});

export default App;
