import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'
import Button from '../components/Button'

const NumBtn = ({ onBtnClick, numbers }) => {
	return (
		<View style={styles.container}>
			<Button onClick={() => onBtnClick(numbers[0])}>{numbers[0]}</Button>
			<Button onClick={() => onBtnClick(numbers[1])}>{numbers[1]}</Button>
			<Button onClick={() => onBtnClick(numbers[2])}>{numbers[2]}</Button>
			<Button onClick={() => onBtnClick(numbers[3])}>{numbers[3]}</Button>
			<Button onClick={() => onBtnClick(numbers[4])}>{numbers[4]}</Button>
			<Button onClick={() => onBtnClick(numbers[5])}>{numbers[5]}</Button>
			<Button onClick={() => onBtnClick(numbers[6])}>{numbers[6]}</Button>
			<Button onClick={() => onBtnClick(numbers[7])}>{numbers[7]}</Button>
			<Button onClick={() => onBtnClick(numbers[8])}>{numbers[8]}</Button>
			<View style={styles.equalContainer}>
				<Button onClick={() => onBtnClick(numbers[9])}>{numbers[9]}</Button>
				<Button style={styles.eq} onClick={() => onBtnClick(numbers[10])}>{numbers[10]}</Button>
			</View>
		</View>
	)
}

export default NumBtn

const styles = StyleSheet.create({
	container: {
		justifyContent: 'space-between',
		flexDirection: 'row-reverse',
		flexWrap: 'wrap',
		width: '75%',
	},
	equalContainer: {
		flexDirection: 'row',
		width: '100%'
	},
	eq: {
		flex: 1
	}
})
