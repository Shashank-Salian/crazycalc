import React from 'react'
import { StyleSheet, View } from 'react-native'
import Button from '../components/Button'

const OtherBtn = ({ onBtnClick }) => {
	return (
		<View style={styles.container}>
			<Button style={styles.btn} onClick={() => onBtnClick('C')}>C</Button>
		</View>
	)
}

export default OtherBtn

const styles = StyleSheet.create({
	container: {
		justifyContent: 'flex-end',
		flexDirection: 'row'
	},
	btn: {
		backgroundColor: '#EE933F',
	}
})
