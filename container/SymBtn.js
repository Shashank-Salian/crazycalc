import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'
import Button from '../components/Button'

const SymBtn = ({ onBtnClick, symbols }) => {
	return (
		<View style={styles.container}>
			<Button style={styles.btn} onClick={() => onBtnClick(symbols[0])}>{symbols[0]}</Button>
			<Button style={styles.btn} onClick={() => onBtnClick(symbols[1])}>{symbols[1]}</Button>
			<Button style={styles.btn} onClick={() => onBtnClick(symbols[2])}>{symbols[2]}</Button>
			<Button style={styles.btn} onClick={() => onBtnClick(symbols[3])}>{symbols[3]}</Button>
		</View>
	)
}

export default SymBtn

const styles = StyleSheet.create({
	container: {
		justifyContent: 'space-between',
		alignItems: 'center'
	},
	btn: {
		backgroundColor: '#EE933F'
	}
})
