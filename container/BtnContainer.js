import React from 'react'
import { StyleSheet, View } from 'react-native'
import NumBtn from './NumBtn'
import OtherBtn from './OtherBtn'
import SymBtn from './SymBtn'

const BtnContainer = ({ onBtnClick, numbers, symbols }) => {
	return (
		<View style={styles.container}>
			<OtherBtn onBtnClick={onBtnClick} />
			<View style={styles.row}>
				<NumBtn onBtnClick={onBtnClick} numbers={numbers} />
				<SymBtn onBtnClick={onBtnClick} symbols={symbols} />
			</View>
		</View>
	)
}

export default BtnContainer

const styles = StyleSheet.create({
	container: {
		width: '100%',
		paddingVertical: 15,
		backgroundColor: '#000',
		paddingHorizontal: 3,
		flex: 1
	},
	row: {
		justifyContent: 'space-between',
		flexDirection: 'row'
	}
})
